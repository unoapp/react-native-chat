import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import Login from './pages/login'
import PageRouter from './pages/page-router'

const routes = {
  Login,
  PageRouter,
}
const navigator = createSwitchNavigator(routes)
const router = createAppContainer(navigator)

export default router