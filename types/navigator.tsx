interface Navigator {
  navigate: Function
}

export interface Navigation {
  navigation: Navigator
}