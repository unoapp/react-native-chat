import React from 'react';
import { StyleSheet, FlatList, Text, SafeAreaView } from 'react-native';
import { ListItem } from 'react-native-elements';
import Button from '../../components/button'
import { withNavigation, NavigationParams } from 'react-navigation';

const Home: React.FC <NavigationParams> = (prop) => {

  const locations = [
    {name: 'test'},
    {name: 'test2'},
    {name: 'test3'}
  ]

  return (
    <SafeAreaView>
      <Text>
        Home page
      </Text>
      <Button
        title="logout"
        onPress={() => prop.navigation.navigate('Login')}
      />
    </SafeAreaView>
  );
}
export default withNavigation(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 35,
  }
});
