import React from 'react';
import { createAppContainer, createDrawerNavigator } from 'react-navigation'
import { Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'

import Locations from './locations'
import Home from './home'
import Chat from './chat'
import Tickets from './tickets'

const routes = {
  Locations: {
    screen: Locations,
    navigationOptions: {
      drawerLabel: 'Locations',
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='map-marker'
          size={30}
          color='#517fa4'
        />
      )
    }
  },
  Home: {
    screen: Home,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='home'
          size={30}
          color='#517fa4'
        />
      )
    }
  },
  Chat: {
    screen: Chat,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='comments'
          size={30}
          color='#517fa4'
        />
      )
    }
  },
  Tickets: {
    screen: Tickets,
    navigationOptions: {
      drawerIcon: ({ tintColor }) => (
        <Icon
          name='ticket'
          size={30}
          color='#517fa4'
        />
      )
    }
  }
}

const drawerConfig = {
  drawerWidth: 350,
  hideStatusBar: true,
}

const navigator = createDrawerNavigator(routes, drawerConfig)
const router = createAppContainer(navigator)

export default router