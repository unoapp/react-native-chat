import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { ListItem } from 'react-native-elements';
import Button from '../../components/button'
import { withNavigation, NavigationParams } from 'react-navigation';

const Locations: React.FC <NavigationParams> = (prop) => {

  const locations = [
    {name: 'test'},
    {name: 'test2'},
    {name: 'test3'}
  ]

  return (
    <>
      <Button
        title="logout"
        onPress={() => prop.navigation.navigate('Login')}
      />
    </>
  );
}
export default withNavigation(Locations);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 35,
  }
});
