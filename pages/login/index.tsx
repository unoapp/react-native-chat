import React, { useState } from 'react'
import { withNavigation, NavigationParams } from 'react-navigation';
// @ts-ignore
import Icon from 'react-native-vector-icons/FontAwesome'
import Button from '../../components/button'

import { StyleSheet, View, Image, KeyboardAvoidingView } from 'react-native'
import { Input } from 'react-native-elements'

const LoginPage: React.FC <NavigationParams> = (prop) => {
  const [username, updateUsername] = useState('')
  const [password, updatePassword] = useState('')

  const login = () => {
    console.log(prop.navigation.navigate('Locations'))
    console.log('login')
    console.log(username, password)
  }
  return (
    <KeyboardAvoidingView behavior="padding" style={styles.container}>
      <Image
        style={{ width: 280, height: 65 }}
        source={require('../../assets/unoapp.png')}
      />
      <Input
        placeholder="Username"
        onChangeText={(string) => updateUsername(string)}
        leftIconContainerStyle={styles.icon}
        leftIcon={
          <Icon
            name='envelope'
            size={24}
            color='black'
          />
        }
      />
      <Input
        placeholder="Password"
        onChangeText={(string) => updatePassword(string)}
        leftIconContainerStyle={styles.icon}
        leftIcon={
          <Icon
            name='lock'
            size={24}
            color='black'
          />
        }
      />
      <Button
        onPress={login}
        title="Login"
      />
    </KeyboardAvoidingView>
  );
}
export default withNavigation(LoginPage)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 35,
  }
})
