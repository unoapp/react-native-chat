import React from 'react';
import { StyleSheet, FlatList, Text, SafeAreaView } from 'react-native';
import { ListItem } from 'react-native-elements';
import Button from '../../components/button'

const Locations: React.FC = () => {

  const locations = [
    {name: 'Legend Headquarters'},
    {name: 'Death Star'},
    {name: 'UNO-chat'}
  ]

  return (
    <SafeAreaView>
      <Text style={styles.title}>
        Select Your Location
      </Text>
      <FlatList
      data={locations}
      renderItem={
        ({item}) => {
          return (
            <Button
              style={styles.button}
              title={item.name}
              onPress={() => {}}
            />
          )
        }
      }
      keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  );
}
export default Locations;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 30,
  },
  button: {
    margin: 10,
  }
});
