import React from 'react';
import { Button } from 'react-native-elements';
import { ViewStyle } from 'react-native';

interface ButtonProps {
  onPress: Function,
  title: string,
  style?: ViewStyle,
}

const button: React.FC <ButtonProps> = ({ onPress, title, style }) => {
  style = {
    backgroundColor: '#4b4b4b',
    margin: 20,
    ...style
  }
  return (
    <Button
      onPress={() => onPress()}
      buttonStyle={style}
      title={title}
    />
  )
}

export default button;